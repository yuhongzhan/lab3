// main.cpp
//
// Main file for simulated conveyor belt
// Initial revision: Donald G Dansereau, 2019
// Completed by:

#include <iostream>
#include <cstdlib>      // rand
#include <algorithm>    // std::max

#include "Conveyor.h"
#include "LoadingRobot.h"
#include "ProcessingRobot.h"


int main()
{
    // Item myItem;
    Conveyor myConveyor;
    LoadingRobot myLoader;
    ProcessingRobot myProcessor;

    myConveyor.Init();
    myLoader.Init( &myConveyor );
    myProcessor.Init( &myConveyor );

    while(1)
    {
    std::cout << "cycle begin" << std::endl;

    myLoader.AddItems();
    myConveyor.Report();
    // myProcessor.ProcessItems();
    myProcessor.ProcessItemsTimed();
    myConveyor.Report();

    std::cout << "cycle end" << std::endl;
    }


}

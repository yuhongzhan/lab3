// ProcessingRobot.cpp
//
// Implementation file for a robot that processes items off a conveyor belt
// Initial revision: Donald G Dansereau
// Completed by:

#include <iostream>
#include <cstdlib>      // rand
#include <algorithm>    // std::max

#include "ProcessingRobot.h"

//---------------------------------------------------------------
void ProcessingRobot::Init( Conveyor* WhichConveyor )
{
    _Conveyor = WhichConveyor;
}

//---------------------------------------------------------------
void ProcessingRobot::ProcessItems()
{
    _Conveyor->RemoveItems( 5 );
}

float ProcessingRobot::mean(std::vector<float> Time_active)
{
  float m = 0;
  for (size_t i = 0; i < Time_active.size(); i++) {
    m += Time_active[i];
  }
  return m/Time_active.size();
}


void ProcessingRobot::ProcessItemsTimed(char mode)
{
        TotalProcessTime = 16.0 ;
        float time_add = 0;
        int num_process = 0;
    if (mode == 'n'){

        while (time_add < TotalProcessTime&&num_process < _Conveyor->length_pointer())
        {
          time_add += _Conveyor->process_1_item(num_process);
          num_process++;
        }
        num_process--;
        // std::cout <<"debug  " <<num_process << '\n';
        num_process = std::min(num_process,_Conveyor->_NumItemsOnConveyor);
        time_add = _Conveyor->RemoveItems( num_process );
        // std::cout << "the percentage of mechine running: "<<time_add/TotalProcessTime*100<<"%" << '\n';
        Time_active.push_back(time_add);
        Time_wait.push_back(TotalProcessTime-time_add);
        std::cout << "the percentage of mechine running: "<<mean(Time_active)/TotalProcessTime*100<<"%" << '\n';
        std::cout << "the time of mechine waiting: "<<mean(Time_wait)<<" s" << '\n';
    }
    else if(mode == 'l')
    {
        while (_Conveyor->length_pointer()>0)
        {
          // check longest item on conveyor and process it
          num_process = _Conveyor->longest_item();
          if (time_add+_Conveyor->process_1_item(num_process)< TotalProcessTime)
          {
              time_add += _Conveyor->process_1_item(num_process);
              _Conveyor->remove_particular_one(num_process);
              num_process++;

          }else
          {
              Time_active.push_back(time_add);
              Time_wait.push_back(TotalProcessTime-time_add);
              std::cout << "the percentage of mechine running: "<<mean(Time_active)/TotalProcessTime*100<<"%" << '\n';
              std::cout << "the time of mechine waiting: "<<mean(Time_wait)<<" s" << '\n';
              return;
          }
        }
    }
    else if(mode == 's')
    {
      while (_Conveyor->length_pointer()>0)
      {
        // check longest item on conveyor and process it
        num_process = _Conveyor->shortest_item();
        if (time_add+_Conveyor->process_1_item(num_process)< TotalProcessTime)
        {
            time_add += _Conveyor->process_1_item(num_process);
            _Conveyor->remove_particular_one(num_process);
            num_process++;

        }else
        {
            Time_active.push_back(time_add);
            Time_wait.push_back(TotalProcessTime-time_add);
            std::cout << "the percentage of mechine running: "<<mean(Time_active)/TotalProcessTime*100<<"%" << '\n';
            std::cout << "the time of mechine waiting: "<<mean(Time_wait)<<" s" << '\n';
            return;
        }
      }
    }
    else
        std::cout << "Wrong input, please input l/s/n" << '\n';






   //std::cout<< "ProcessTime is " << ProcessTime -> back() << std::endl;

    //_ProcessTime = _Item -> GetTime();
    //
    // std::cout<<_ProcessTime->size()<<std::endl;

    // for (int i=0; i< ProcessTime -> size() ; i++)
    // {
     //std::cout<< "ProcessTime is" << _ProcessTime -> back () << std::endl;
    // }
}

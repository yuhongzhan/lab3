// Conveyor.h
//
// Header file for a simulated conveyor belt
// Initial revision: Donald G Dansereau, 2019
// Completed by:

#ifndef _CONVEYOR_H
#define _CONVEYOR_H

#include <iostream>     // std::cout
#include <algorithm>    // std::max
#include <vector>

#include "Item.h"

//---------------------------------------------------------------
// Simulate a conveyor belt. This version just counts how many
// objects are on the belt, and accepts requests to add and remove
// objects.
class Conveyor
{
    public:
        void Init();
        void AddItems( int n );
        int longest_item();
        int shortest_item();
        float RemoveItems( int n );
        void Report();
        float process_1_item(int i);
        float remove_particular_one(int i);
        int length_pointer();
        std::vector<Item*> Item_list;
        int _NumItemsOnConveyor;
    private:

        int _Itemtrack;
        Item* _Item;
};



#endif

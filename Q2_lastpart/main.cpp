// main.cpp
//
// Main file for simulated conveyor belt
// Initial revision: Donald G Dansereau, 2019
// Completed by:

#include <iostream>
#include <cstdlib>      // rand
#include <algorithm>    // std::max

#include "Conveyor.h"
#include "LoadingRobot.h"
#include "ProcessingRobot.h"


int main()
{
    // Item myItem;
    Conveyor myConveyor;
    LoadingRobot myLoader;
    ProcessingRobot myProcessor;

    myConveyor.Init();
    myLoader.Init( &myConveyor );
    myProcessor.Init( &myConveyor );

    char mode;
//  input a running mode
    while (mode!='l' && mode!='s' && mode!='n')
    {
      std::cout << "Please select running mode:\ns-mechine taking shortest time item"
      <<"\nl-mechine taking longest time item" <<
      "\nn-mechine will process the first item at proceesing port"<< std::endl;
      std::cin >> mode;
    }
    // run the code infinitely accroding to users input mode
    while(1)
    {
    std::cout << "cycle begin" << std::endl;
    myLoader.AddItems();
    myConveyor.Report();
    // myProcessor.ProcessItems();s
    myProcessor.ProcessItemsTimed(mode);
    myConveyor.Report();
    // std::cout << "longest item is number "<< myConveyor.longest_item() << '\n';
    std::cout << "cycle end" << std::endl;
    }



}

// Conveyor.cpp
//
// Implementation file for a simulated conveyor belt
// Initial revision: Donald G Dansereau, 2019
// Completed by:

#include "Conveyor.h"

//---------------------------------------------------------------
void Conveyor::Init()
{
    _NumItemsOnConveyor = 0;
    _Itemtrack = 0;
}

//---------------------------------------------------------------
void Conveyor::AddItems( int n )
{
    _NumItemsOnConveyor += n;
    // std::cout <<n << " New Items add" << '\n';


    for(int i=_Itemtrack; i<_Itemtrack +n; i++)
    {
        Item_list.push_back(new Item(i));
        //std::cout << "The time is "<<Item_list.back()->_ProcessTime << '\n';
        // float time = _Item->initID(_Itemtrack);
        // Item_on_it.push_back(_Item);
        // std::cout << "Items Process address "<< Item_on_it.back << '\n';
    }
    _Itemtrack +=n;
}

//---------------------------------------------------------------
float Conveyor::RemoveItems( int n )
{

    // Note that we cannot have a negative number of items on belt
    _NumItemsOnConveyor = std::max(0, _NumItemsOnConveyor-n);

    //Report the IDs of items as they are removed from the conveyor
   // std::cout <<n<< " Items removed: " << '\n';
    float time = 0;
    for (size_t i = 0; i < n; i++)
    {
      time += Item_list.front()->_ProcessTime;
      std::cout << "Item ID number is "<<Item_list.front()->_ID << '\n';
      // delete Item;
      Item_list.erase(Item_list.begin());
    }

    // std::cout << "All time need is : "<< time << '\n';
    return time;

}
float Conveyor::remove_particular_one(int i)
{
    float time = 0;
    time += Item_list[i]->_ProcessTime;
    std::cout << "Remove Item "<<Item_list[i]->_ID <<"\nIt cost time:"
    <<Item_list[i]->_ProcessTime<< '\n';
    // delete Item;
    _NumItemsOnConveyor -= 1;
    Item_list.erase(Item_list.begin()+i);
    return time;
}
//---------------------------------------------------------------
float Conveyor::process_1_item(int i)
{
  return Item_list[i]->_ProcessTime;
}

int Conveyor::length_pointer()
{
  return Item_list.size();
}

int Conveyor::longest_item()
{
  float max_time = 0;
  int index = 0;
  for (size_t i = 0; i < Item_list.size(); i++)
  {
    if (Item_list[i]->_ProcessTime>max_time)
    {
      max_time = Item_list[i]->_ProcessTime;
      index = i;
    }
  }
  return index;
}

int Conveyor::shortest_item()
{
  float min_time = 0xff;
  int index = 0;
  for (size_t i = 0; i < Item_list.size(); i++)
  {
    if (Item_list[i]->_ProcessTime<min_time)
    {
      min_time = Item_list[i]->_ProcessTime;
      index = i;
    }
  }
  return index;
}

//---------------------------------------------------------------
void Conveyor::Report()
{
    std::cout << "Items on conveyor: " << _NumItemsOnConveyor << std::endl;
}

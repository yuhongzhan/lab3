// Person.h
//
// This is the header file for Person class

#ifndef _PERSON_H
#define _PERSON_H

//---Class definition for Person-------------------------------
// This defines the interface for this class
class Person
{
    public:
        // initialise a person with a given name
        //void Init( std::string Name );
        Person(std::string Name);

        // say hello to another person;
        // the second person is specified using a pointer
        void SendHello( Person* pDest );

        // report hello count & hello received
        void Report(Person* pDest);

        // received hello from a Person
        void RecieveHello(Person* pSource);

        // the name of the person
        std::string _name;

        // How many hello received from others
        int _helloReceived;

    private:
        // How many times someone's said hello to this person
        int _helloCount;

};

#endif

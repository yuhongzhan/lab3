// Computer.cpp
//
// Implementation file for the computer

#include <iostream>
#include <string>

#include "Person.h"
#include "Computer.h"

//---Class implementation for Computer---------------------------
//Init the name of the computer and output it to the screen
Computer::Computer(std::string Name)
{
    _comname = Name;
    std::cout << "[Computer]:" << _comname << std::endl;
}

// SendHello allows the computer to say hello to the Person
// the class do not have any hello counting or receivehello functionality
void Computer::SendHello( Person* pDest )
{
    std::cout << "[SendHello]: " << _comname
    << " is saying hello to " << pDest->_name << std::endl;

    pDest->_helloReceived++; //Adding hello count for the people
}

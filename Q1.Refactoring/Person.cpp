// Computer.cpp
//
// Implementation file for the person
// Including initializing and sending/receving hello
// Reporter is the person who report the total count

#include <iostream>
#include <string>

#include "Person.h"
#include "Computer.h"

//---Class implementation for Person---------------------------
// Init zeroes the hello count, and assigns a name
// it also outputs to the screen so we can see what's going on
Person::Person(std::string Name):_helloCount(0),_helloReceived(0)
{
    _name = Name;
    std::cout << "[Init]: " << _name << " is initialised with "
        << _helloCount << " hellos" << std::endl;
}

//-------------------------------------------------------------
// SendHello says hello to another person by incrementing their
// hello count.  It also outputs information to the screen so
// we can see what's going on.
void Person::SendHello( Person* pDest )
{

    if (this != pDest)
    {
        std::cout << "[SendHello]: " << _name
            << " is saying hello to " << pDest->_name << std::endl;

        pDest->_helloReceived++; //helloreceived add one for the people second people

        this->_helloCount++;  //hellocount add on for the first people

        this->RecieveHello(pDest); //immediatetly say hello back to the first people

    }
    else
    {
      //output error meassage when people say hello to themselves
      std::cout << _name <<" don't say hello to myself"<< std::endl;
    }

}

// The function knows who's saying hello and automatically reciprocates by saying hello back
// Output it to the screen
void Person::RecieveHello(Person* pSource)
{
      std::cout << "[RecievedHello]: " << pSource->_name
          << " say hello back to "<< _name << std::endl;
      this->_helloReceived++;  //helloreceived add one for the people first people
      pSource->_helloCount++; //hellocount add on for the second people
}

// The reporter repot how many hellos the person has received
// and how many hello the person has said
void Person::Report(Person* pDest)
{
    std::cout << "Reporter: " << pDest->_name
      << "'s _helloCount is now " << pDest->_helloCount
      << std::endl;
    std::cout << "Reporter: " << pDest->_name
      << "'s _helloRecevied is now " << pDest->_helloReceived
      << std::endl;
}

// Computer.h
//
// This is the header file for Computer class

#ifndef _COMPUTER_H
#define _COMPUTER_H

//---Class definition for Computer-------------------------------
// This defines the interface for this class
#include "Person.h"
class Computer
{
    public:
        //Initialise the name of the computer
        // void Init( std::string Name );
         Computer(std::string Name);

        //say hello to the person
        void SendHello( Person* pDest );

    private:
        //The computer's name
        std::string _comname;
        Person* _Person;

};
#endif

// HelloClasses.cpp
//
// Example class with private members and public methods
// Each person keeps track of how many times they're said hello to
// A person can say hello to another person
//

// Spliting the question 2 into multiple files
// There are two .h files and two .cpp files

#include <iostream>
#include <string>

// Include the header files for person and computer
#include"Person.h"
#include"Computer.h"

//-------------------------------------------------------------
int main()
{
    Person Alice("Alice") ;// Alice is an instance of the class Person
    Person Bob("Bob"); // Bob is another instance of Person
    Person Carol("Carol");// Carol is the third person
    Person Reporter("Reporter"); // The reporter count times

    // Let's introduce Alice and Bob
    Alice.SendHello( &Bob );

    // Introduce Alice and bob to themselves
    Alice.SendHello( &Alice );
    Bob.SendHello( &Bob );

    // Add thid person Carol and introduce Carol to Alice and Bob
    Carol.SendHello(&Alice);
    Carol.SendHello(&Bob);

    // Instantiating a computer named Hal
    Computer Hal("Hal");

    // Let Hal say hello to Carol
    Hal.SendHello(&Carol);

    // Report the time they has benn helloed and send hellos
    Reporter.Report(&Carol);
    Reporter.Report(&Bob);
    Reporter.Report(&Alice);

}
